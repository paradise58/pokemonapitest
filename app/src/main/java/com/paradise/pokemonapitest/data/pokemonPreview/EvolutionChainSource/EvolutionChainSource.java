package com.paradise.pokemonapitest.data.pokemonPreview.EvolutionChainSource;

import android.util.Log;
import com.paradise.pokemonapitest.data.Pokemon;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.paradise.pokemonapitest.data.pokemonPreview.PokemonPreview;

public class EvolutionChainSource  {

  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("chain")
  @Expose
  private Chain chain;

  public Integer getId() {
    return id;
  }

  public Chain getChain() {
    return chain;
  }


}

