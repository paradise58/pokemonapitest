package com.paradise.pokemonapitest.data.pokemonInfo.AbilitySource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EffectEntry {

  @SerializedName("effect")
  @Expose
  private String effect;
  @SerializedName("short_effect")
  @Expose
  private String shortEffect;

  public String getEffect() {
    return effect;
  }

  public String getShortEffect() {
    return shortEffect;
  }

}
