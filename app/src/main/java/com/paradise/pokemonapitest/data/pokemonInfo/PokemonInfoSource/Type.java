package com.paradise.pokemonapitest.data.pokemonInfo.PokemonInfoSource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Type {
  @SerializedName("slot") @Expose private Integer slot;
  @SerializedName("type") @Expose private Type_ type;

  public Integer getSlot() {
    return slot;
  }

  public Type_ getType() {
    return type;
  }

  public void setSlot(Integer slot) {
      this.slot = slot;
  }


}
