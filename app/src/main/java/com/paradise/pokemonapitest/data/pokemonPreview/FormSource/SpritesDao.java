package com.paradise.pokemonapitest.data.pokemonPreview.FormSource;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

import com.paradise.pokemonapitest.data.DaoSession;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "SPRITES".
*/
public class SpritesDao extends AbstractDao<Sprites, String> {

    public static final String TABLENAME = "SPRITES";

    /**
     * Properties of entity Sprites.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property PokeName = new Property(0, String.class, "pokeName", true, "POKE_NAME");
        public final static Property BackDefault = new Property(1, String.class, "backDefault", false, "BACK_DEFAULT");
        public final static Property BackShiny = new Property(2, String.class, "backShiny", false, "BACK_SHINY");
        public final static Property FrontDefault = new Property(3, String.class, "frontDefault", false, "FRONT_DEFAULT");
        public final static Property FrontShiny = new Property(4, String.class, "frontShiny", false, "FRONT_SHINY");
    }


    public SpritesDao(DaoConfig config) {
        super(config);
    }
    
    public SpritesDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"SPRITES\" (" + //
                "\"POKE_NAME\" TEXT PRIMARY KEY NOT NULL ," + // 0: pokeName
                "\"BACK_DEFAULT\" TEXT," + // 1: backDefault
                "\"BACK_SHINY\" TEXT," + // 2: backShiny
                "\"FRONT_DEFAULT\" TEXT," + // 3: frontDefault
                "\"FRONT_SHINY\" TEXT);"); // 4: frontShiny
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"SPRITES\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, Sprites entity) {
        stmt.clearBindings();
 
        String pokeName = entity.getPokeName();
        if (pokeName != null) {
            stmt.bindString(1, pokeName);
        }
 
        String backDefault = entity.getBackDefault();
        if (backDefault != null) {
            stmt.bindString(2, backDefault);
        }
 
        String backShiny = entity.getBackShiny();
        if (backShiny != null) {
            stmt.bindString(3, backShiny);
        }
 
        String frontDefault = entity.getFrontDefault();
        if (frontDefault != null) {
            stmt.bindString(4, frontDefault);
        }
 
        String frontShiny = entity.getFrontShiny();
        if (frontShiny != null) {
            stmt.bindString(5, frontShiny);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, Sprites entity) {
        stmt.clearBindings();
 
        String pokeName = entity.getPokeName();
        if (pokeName != null) {
            stmt.bindString(1, pokeName);
        }
 
        String backDefault = entity.getBackDefault();
        if (backDefault != null) {
            stmt.bindString(2, backDefault);
        }
 
        String backShiny = entity.getBackShiny();
        if (backShiny != null) {
            stmt.bindString(3, backShiny);
        }
 
        String frontDefault = entity.getFrontDefault();
        if (frontDefault != null) {
            stmt.bindString(4, frontDefault);
        }
 
        String frontShiny = entity.getFrontShiny();
        if (frontShiny != null) {
            stmt.bindString(5, frontShiny);
        }
    }

    @Override
    public String readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0);
    }    

    @Override
    public Sprites readEntity(Cursor cursor, int offset) {
        Sprites entity = new Sprites( //
            cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0), // pokeName
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // backDefault
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // backShiny
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // frontDefault
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4) // frontShiny
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, Sprites entity, int offset) {
        entity.setPokeName(cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0));
        entity.setBackDefault(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setBackShiny(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setFrontDefault(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setFrontShiny(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
     }
    
    @Override
    protected final String updateKeyAfterInsert(Sprites entity, long rowId) {
        return entity.getPokeName();
    }
    
    @Override
    public String getKey(Sprites entity) {
        if(entity != null) {
            return entity.getPokeName();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(Sprites entity) {
        return entity.getPokeName() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
