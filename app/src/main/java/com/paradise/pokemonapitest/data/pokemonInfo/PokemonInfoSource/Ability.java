package com.paradise.pokemonapitest.data.pokemonInfo.PokemonInfoSource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ability {

  @SerializedName("is_hidden")
  @Expose
  private Boolean isHidden;
  @SerializedName("slot")
  @Expose
  private Integer slot;
  @SerializedName("ability")
  @Expose
  private Ability_ ability;

  public Boolean getIsHidden() {
    return isHidden;
  }

  public Integer getSlot() {
    return slot;
  }

  public Ability_ getAbility() {
    return ability;
  }

}
