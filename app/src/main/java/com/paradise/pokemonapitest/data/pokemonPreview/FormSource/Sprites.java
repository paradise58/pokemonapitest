package com.paradise.pokemonapitest.data.pokemonPreview.FormSource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Sprites {
  @Id
  String pokeName;
  @SerializedName("back_default") @Expose private String backDefault;
  @SerializedName("back_shiny") @Expose private String backShiny;
  @SerializedName("front_default") @Expose private String frontDefault;
  @SerializedName("front_shiny") @Expose private String frontShiny;

  @Generated(hash = 1855959924)
  public Sprites() {
  }





  @Generated(hash = 1085893806)
  public Sprites(String pokeName, String backDefault, String backShiny,
          String frontDefault, String frontShiny) {
      this.pokeName = pokeName;
      this.backDefault = backDefault;
      this.backShiny = backShiny;
      this.frontDefault = frontDefault;
      this.frontShiny = frontShiny;
  }





  public String getBackDefault() {
    return backDefault;
  }

  public void setBackDefault(String backDefault) {
    this.backDefault = backDefault;
  }

  public String getBackShiny() {
    return backShiny;
  }

  public void setBackShiny(String backShiny) {
    this.backShiny = backShiny;
  }

  public String getFrontDefault() {
    return frontDefault;
  }

  public void setFrontDefault(String frontDefault) {
    this.frontDefault = frontDefault;
  }

  public String getFrontShiny() {
    return frontShiny;
  }

  public void setFrontShiny(String frontShiny) {
    this.frontShiny = frontShiny;
  }

  public void setPokeName(String pokeName) {
    this.pokeName = pokeName;
  }





  public String getPokeName() {
      return this.pokeName;
  }
}