package com.paradise.pokemonapitest.data;

import java.util.Map;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import org.greenrobot.greendao.internal.DaoConfig;

import com.paradise.pokemonapitest.data.Form;
import com.paradise.pokemonapitest.data.Pokemon;
import com.paradise.pokemonapitest.data.pokemonPreview.FormSource.Sprites;
import com.paradise.pokemonapitest.data.Skill;

import com.paradise.pokemonapitest.data.FormDao;
import com.paradise.pokemonapitest.data.PokemonDao;
import com.paradise.pokemonapitest.data.pokemonPreview.FormSource.SpritesDao;
import com.paradise.pokemonapitest.data.SkillDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see org.greenrobot.greendao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig formDaoConfig;
    private final DaoConfig pokemonDaoConfig;
    private final DaoConfig spritesDaoConfig;
    private final DaoConfig skillDaoConfig;

    private final FormDao formDao;
    private final PokemonDao pokemonDao;
    private final SpritesDao spritesDao;
    private final SkillDao skillDao;

    public DaoSession(Database db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        formDaoConfig = daoConfigMap.get(FormDao.class).clone();
        formDaoConfig.initIdentityScope(type);

        pokemonDaoConfig = daoConfigMap.get(PokemonDao.class).clone();
        pokemonDaoConfig.initIdentityScope(type);

        spritesDaoConfig = daoConfigMap.get(SpritesDao.class).clone();
        spritesDaoConfig.initIdentityScope(type);

        skillDaoConfig = daoConfigMap.get(SkillDao.class).clone();
        skillDaoConfig.initIdentityScope(type);

        formDao = new FormDao(formDaoConfig, this);
        pokemonDao = new PokemonDao(pokemonDaoConfig, this);
        spritesDao = new SpritesDao(spritesDaoConfig, this);
        skillDao = new SkillDao(skillDaoConfig, this);

        registerDao(Form.class, formDao);
        registerDao(Pokemon.class, pokemonDao);
        registerDao(Sprites.class, spritesDao);
        registerDao(Skill.class, skillDao);
    }
    
    public void clear() {
        formDaoConfig.clearIdentityScope();
        pokemonDaoConfig.clearIdentityScope();
        spritesDaoConfig.clearIdentityScope();
        skillDaoConfig.clearIdentityScope();
    }

    public FormDao getFormDao() {
        return formDao;
    }

    public PokemonDao getPokemonDao() {
        return pokemonDao;
    }

    public SpritesDao getSpritesDao() {
        return spritesDao;
    }

    public SkillDao getSkillDao() {
        return skillDao;
    }

}
