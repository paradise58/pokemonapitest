package com.paradise.pokemonapitest.data.pokemonPreview.EvolutionChainSource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class Chain {

  @SerializedName("is_baby")
  @Expose
  private Boolean isBaby;
  @SerializedName("species")
  @Expose
  private Species species;
  @SerializedName("evolution_details")
  @Expose
  private Object evolutionDetails;
  @SerializedName("evolves_to")
  @Expose
  private List<EvolvesTo> evolvesTo = null;

  public Boolean getIsBaby() {
    return isBaby;
  }

  public void setIsBaby(Boolean isBaby) {
    this.isBaby = isBaby;
  }

  public Species getSpecies() {
    return species;
  }

  public void setSpecies(Species species) {
    this.species = species;
  }

  public Object getEvolutionDetails() {
    return evolutionDetails;
  }

  public void setEvolutionDetails(Object evolutionDetails) {
    this.evolutionDetails = evolutionDetails;
  }

  public List<EvolvesTo> getEvolvesTo() {
    return evolvesTo;
  }

  public void setEvolvesTo(List<EvolvesTo> evolvesTo) {
    this.evolvesTo = evolvesTo;
  }

}
