package com.paradise.pokemonapitest.data.pokemonInfo.PokemonInfoSource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.paradise.pokemonapitest.data.Pokemon;
import com.paradise.pokemonapitest.data.pokemonPreview.FormSource.Sprites;
import java.util.List;

public class PokemonInfoSource {

  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("base_experience")
  @Expose
  private Integer baseExperience;
  @SerializedName("height")
  @Expose
  private Integer height;
  @SerializedName("is_default")
  @Expose
  private Boolean isDefault;
  @SerializedName("order")
  @Expose
  private Integer order;
  @SerializedName("weight")
  @Expose
  private Integer weight;
  @SerializedName("abilities")
  @Expose
  private List<Ability> abilities = null;
  @SerializedName("species")
  @Expose
  private Species species;
  @SerializedName("sprites")
  @Expose
  private Sprites sprites;
  @SerializedName("stats")
  @Expose
  private List<Stat> stats = null;
  @SerializedName("types")
  @Expose
  private List<Type> types = null;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getBaseExperience() {
    return baseExperience;
  }

  public void setBaseExperience(Integer baseExperience) {
    this.baseExperience = baseExperience;
  }

  public Integer getHeight() {
    return height;
  }

  public void setHeight(Integer height) {
    this.height = height;
  }

  public Boolean getIsDefault() {
    return isDefault;
  }

  public void setIsDefault(Boolean isDefault) {
    this.isDefault = isDefault;
  }

  public Integer getOrder() {
    return order;
  }

  public void setOrder(Integer order) {
    this.order = order;
  }

  public Integer getWeight() {
    return weight;
  }

  public void setWeight(Integer weight) {
    this.weight = weight;
  }

  public List<Ability> getAbilities() {
    return abilities;
  }

  public void setAbilities(List<Ability> abilities) {
    this.abilities = abilities;
  }


  public Species getSpecies() {
    return species;
  }

  public void setSpecies(Species species) {
    this.species = species;
  }

  public Sprites getSprites() {
    return sprites;
  }

  public void setSprites(Sprites sprites) {
    this.sprites = sprites;
  }

  public List<Stat> getStats() {
    return stats;
  }

  public void setStats(List<Stat> stats) {
    this.stats = stats;
  }

  public List<Type> getTypes() {
    return types;
  }

  public void setTypes(List<Type> types) {
    this.types = types;
  }

}
