package com.paradise.pokemonapitest.data.pokemonInfo.AbilitySource;

import android.util.Log;
import com.paradise.pokemonapitest.data.Pokemon;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AbilitySource  {

  @SerializedName("id") @Expose private Integer id;
  @SerializedName("name") @Expose private String name;
  @SerializedName("effect_entries") @Expose private List<EffectEntry> effectEntries = null;
  @SerializedName("flavor_text_entries") @Expose private List<FlavorTextEntry> flavorTextEntries =
      null;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }


  public List<EffectEntry> getEffectEntries() {
    return effectEntries;
  }

  public List<FlavorTextEntry> getFlavorTextEntries() {
    return flavorTextEntries;
  }

}

