package com.paradise.pokemonapitest.data.pokemonPreview.FormSource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.paradise.pokemonapitest.data.pokemonPreview.PokemonPreview;

public class FormSource  {

  @SerializedName("id") @Expose private Integer id;
  @SerializedName("sprites") @Expose private Sprites sprites;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Sprites getSprites() {
    return sprites;
  }

  public void setSprites(Sprites sprites) {
    this.sprites = sprites;
  }

}