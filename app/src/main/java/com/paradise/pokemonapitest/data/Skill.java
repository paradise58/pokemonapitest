package com.paradise.pokemonapitest.data;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.NotNull;

@Entity(active = true) public class Skill {
@Id String name;
  @NotNull String  flavorTextEntries, effectEntries;

  int users = 1;
  /** Used to resolve relations */
  @Generated(hash = 2040040024)
  private transient DaoSession daoSession;
  /** Used for active entity operations. */
  @Generated(hash = 352229263)
  private transient SkillDao myDao;



  @Generated(hash = 1993658816) public Skill() {
  }

  public int getUsers() {
    return users;
  }

  public void setUsers(int users) {
    this.users = users;
  }

  @Generated(hash = 2077779345)
  public Skill(String name, @NotNull String flavorTextEntries,
          @NotNull String effectEntries, int users) {
      this.name = name;
      this.flavorTextEntries = flavorTextEntries;
      this.effectEntries = effectEntries;
      this.users = users;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFlavorTextEntries() {
    return flavorTextEntries;
  }

  public void setFlavorTextEntries(String flavorTextEntries) {
    this.flavorTextEntries = flavorTextEntries;
  }

  public String getEffectEntries() {
    return effectEntries;
  }

  public void setEffectEntries(String effectEntries) {
    this.effectEntries = effectEntries;
  }

  /**
   * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
   * Entity must attached to an entity context.
   */
  @Generated(hash = 128553479)
  public void delete() {
      if (myDao == null) {
          throw new DaoException("Entity is detached from DAO context");
      }
      myDao.delete(this);
  }

  /**
   * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
   * Entity must attached to an entity context.
   */
  @Generated(hash = 1942392019)
  public void refresh() {
      if (myDao == null) {
          throw new DaoException("Entity is detached from DAO context");
      }
      myDao.refresh(this);
  }

  /**
   * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
   * Entity must attached to an entity context.
   */
  @Generated(hash = 713229351)
  public void update() {
      if (myDao == null) {
          throw new DaoException("Entity is detached from DAO context");
      }
      myDao.update(this);
  }

  /** called by internal mechanisms, do not call yourself. */
  @Generated(hash = 523253144)
  public void __setDaoSession(DaoSession daoSession) {
      this.daoSession = daoSession;
      myDao = daoSession != null ? daoSession.getSkillDao() : null;
  }


}
