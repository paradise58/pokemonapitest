package com.paradise.pokemonapitest.data.pokemonInfo.PokemonInfoSource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.greenrobot.greendao.annotation.Generated;

public class Type_ {
  @SerializedName("name") @Expose private String name;
  @SerializedName("url") @Expose private String url;

  @Generated(hash = 17584648)
  public Type_() {
  }

  public String getName() {
    return name;
  }

  public String getUrl() {
    return url;
  }

  public void setName(String name) {
      this.name = name;
  }

  public void setUrl(String url) {
      this.url = url;
  }

}
