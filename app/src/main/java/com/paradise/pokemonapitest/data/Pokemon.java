package com.paradise.pokemonapitest.data;

import com.paradise.pokemonapitest.common.GreenConverter;
import com.paradise.pokemonapitest.data.pokemonPreview.FormSource.Sprites;
import com.paradise.pokemonapitest.data.pokemonPreview.PokemonPreview;
import java.util.List;
import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinProperty;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Transient;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;
import com.paradise.pokemonapitest.data.pokemonPreview.FormSource.SpritesDao;

@Entity public class Pokemon extends PokemonPreview {
  @Id @NotNull private String pokeName;
  @NotNull private int weight;
  @NotNull private int defence, speed, attack, hp, chainId;
  @ToOne(joinProperty = "pokeName") private Sprites sprites;
  @Transient private List<Skill> skills = null;

  @ToMany(joinProperties = { @JoinProperty(name = "chainId", referencedName = "chainId") })
  private List<Form> forms = null;
  @Convert(converter = GreenConverter.class, columnType = String.class) private List<String> skillNameList;
  @Convert(converter = GreenConverter.class, columnType = String.class) private List<String> typeList;
  /** Used to resolve relations */
  @Generated(hash = 2040040024) private transient DaoSession daoSession;
  /** Used for active entity operations. */
  @Generated(hash = 1047668923) private transient PokemonDao myDao;
  @Generated(hash = 2001041607) private transient String sprites__resolvedKey;

  public List<Skill> getSkills() {
    return skills;
  }

  public List<String> getSkillNameList() {
    return skillNameList;
  }

  public void setSkillNameList(List<String> skillNameList) {
    this.skillNameList = skillNameList;
  }

  public List<String> getTypeList() {
    return typeList;
  }

  public void setTypeList(List<String> typeList) {
    this.typeList = typeList;
  }

  public Pokemon() {
  }

  public Pokemon(String name, int chainId) {
    super(name, chainId);
    pokeName = name;
    this.chainId = chainId;
  }

  @Generated(hash = 228924301)
  public Pokemon(@NotNull String pokeName, int weight, int defence, int speed, int attack, int hp,
      int chainId, List<String> skillNameList, List<String> typeList) {
    this.pokeName = pokeName;
    this.weight = weight;
    this.defence = defence;
    this.speed = speed;
    this.attack = attack;
    this.hp = hp;
    this.chainId = chainId;
    this.skillNameList = skillNameList;
    this.typeList = typeList;
  }

  public void setSkills(List<Skill> skills) {
    this.skills = skills;
  }

  public void setForms(List<Form> forms) {
    this.forms = forms;
  }

  public int getDefence() {
    return defence;
  }

  public void setDefence(int defence) {
    this.defence = defence;
  }

  public int getSpeed() {
    return speed;
  }

  public void setSpeed(int speed) {
    this.speed = speed;
  }

  public int getAttack() {
    return attack;
  }

  public void setAttack(int attack) {
    this.attack = attack;
  }

  public int getHp() {
    return hp;
  }

  public void setHp(int hp) {
    this.hp = hp;
  }

  public int getWeight() {
    return weight;
  }

  public void setWeight(int weight) {
    this.weight = weight;
  }

  public String getPokeName() {
    return this.pokeName;
  }

  public void setPokeName(String pokeName) {
    this.pokeName = pokeName;
  }

  public int getChainId() {
    return chainId;
  }

  public void setChainId(int chainId) {
    this.chainId = chainId;
  }



  /** Resets a to-many relationship, making the next get call to query for a fresh result. */
  @Generated(hash = 949350533) public synchronized void resetForms() {
    forms = null;
  }

  /**
   * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
   * Entity must attached to an entity context.
   */
  @Generated(hash = 128553479) public void delete() {
    if (myDao == null) {
      throw new DaoException("Entity is detached from DAO context");
    }
    myDao.delete(this);
  }

  /**
   * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
   * Entity must attached to an entity context.
   */
  @Generated(hash = 1942392019) public void refresh() {
    if (myDao == null) {
      throw new DaoException("Entity is detached from DAO context");
    }
    myDao.refresh(this);
  }

  /**
   * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
   * Entity must attached to an entity context.
   */
  @Generated(hash = 713229351) public void update() {
    if (myDao == null) {
      throw new DaoException("Entity is detached from DAO context");
    }
    myDao.update(this);
  }

  /** To-one relationship, resolved on first access. */
  @Generated(hash = 1639270942) public Sprites getSprites() {
    String __key = this.pokeName;
    if (sprites__resolvedKey == null || sprites__resolvedKey != __key) {
      final DaoSession daoSession = this.daoSession;
      if (daoSession == null) {
        throw new DaoException("Entity is detached from DAO context");
      }
      SpritesDao targetDao = daoSession.getSpritesDao();
      Sprites spritesNew = targetDao.load(__key);
      synchronized (this) {
        sprites = spritesNew;
        sprites__resolvedKey = __key;
      }
    }
    return sprites;
  }

  /** called by internal mechanisms, do not call yourself. */
  @Generated(hash = 1591532338) public void setSprites(@NotNull Sprites sprites) {
    if (sprites == null) {
      throw new DaoException(
          "To-one property 'pokeName' has not-null constraint; cannot set to-one to null");
    }
    synchronized (this) {
      this.sprites = sprites;
      pokeName = sprites.getPokeName();
      sprites__resolvedKey = pokeName;
    }
  }

  /** called by internal mechanisms, do not call yourself. */
  @Generated(hash = 783325239)
  public void __setDaoSession(DaoSession daoSession) {
      this.daoSession = daoSession;
      myDao = daoSession != null ? daoSession.getPokemonDao() : null;
  }

  /**
   * To-many relationship, resolved on first access (and after reset).
   * Changes to to-many relations are not persisted, make changes to the target entity.
   */
  @Generated(hash = 1736784411)
  public List<Form> getForms() {
      if (forms == null) {
          final DaoSession daoSession = this.daoSession;
          if (daoSession == null) {
              throw new DaoException("Entity is detached from DAO context");
          }
          FormDao targetDao = daoSession.getFormDao();
          List<Form> formsNew = targetDao._queryPokemon_Forms(chainId);
          synchronized (this) {
              if (forms == null) {
                  forms = formsNew;
              }
          }
      }
      return forms;
  }
}




