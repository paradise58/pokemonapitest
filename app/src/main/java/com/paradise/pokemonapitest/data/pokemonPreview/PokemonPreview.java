package com.paradise.pokemonapitest.data.pokemonPreview;

import android.util.Log;
import com.paradise.pokemonapitest.data.pokemonPreview.FormSource.Sprites;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class PokemonPreview {
  public int chainId;
  public String pokeName;
  public Sprites sprites;

  public PokemonPreview(String name) {
    Log.d(TAG, "PokemonPreview: " + name);
    this.pokeName = name;
  }

  public PokemonPreview() {
    Log.d(TAG, "PokemonPreview: " );

  }

  public PokemonPreview(String name, int chainId) {
    Log.d(TAG, "PokemonPreview: " + name + "     " + chainId);

    this.pokeName = name;
    this.chainId = chainId;
  }

  public int getChainId() {
    return chainId;
  }

  public void setChainId(int chainId) {
    this.chainId = chainId;
  }

  public String getPokeName() {
    return pokeName;
  }

  public void setName(String name) {
    this.pokeName = name;
  }

  public Sprites getSprites() {
    return sprites;
  }

  public void setSprites(Sprites sprites) {
    this.sprites = sprites;
  }
}
