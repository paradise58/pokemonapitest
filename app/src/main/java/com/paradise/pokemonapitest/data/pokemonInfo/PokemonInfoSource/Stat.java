package com.paradise.pokemonapitest.data.pokemonInfo.PokemonInfoSource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stat {

  @SerializedName("base_stat") @Expose private Integer baseStat;
  @SerializedName("effort") @Expose private Integer effort;
  @SerializedName("stat") @Expose private Stat_ stat;

  public Integer getBaseStat() {
    return baseStat;
  }

  public Integer getEffort() {
    return effort;
  }


  public Stat_ getStat() {
    return stat;
  }

}
