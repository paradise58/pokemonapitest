package com.paradise.pokemonapitest.data;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

@Entity(active = true) public class Form {
  @Id (autoincrement = true) Long id;
  int chainId;
  int gen;
  String formName;
  /** Used to resolve relations */
  @Generated(hash = 2040040024)
  private transient DaoSession daoSession;
  /** Used for active entity operations. */
  @Generated(hash = 2050372368)
  private transient FormDao myDao;

  public Form(int gen, String formName) {
    this.gen = gen;
    this.formName = formName;
  }


  @Generated(hash = 535210737)
  public Form() {
  }


  @Generated(hash = 871602926)
  public Form(Long id, int chainId, int gen, String formName) {
      this.id = id;
      this.chainId = chainId;
      this.gen = gen;
      this.formName = formName;
  }


  public int getGen() {
    return gen;
  }

  public void setGen(int gen) {
    this.gen = gen;
  }

  public int getChainId() {
    return chainId;
  }

  public void setChainId(int chainId) {
    this.chainId = chainId;
  }

  public String getFormName() {
    return formName;
  }

  public void setFormName(String formName) {
    this.formName = formName;
  }


  /**
   * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
   * Entity must attached to an entity context.
   */
  @Generated(hash = 128553479)
  public void delete() {
      if (myDao == null) {
          throw new DaoException("Entity is detached from DAO context");
      }
      myDao.delete(this);
  }

  /**
   * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
   * Entity must attached to an entity context.
   */
  @Generated(hash = 1942392019)
  public void refresh() {
      if (myDao == null) {
          throw new DaoException("Entity is detached from DAO context");
      }
      myDao.refresh(this);
  }

  /**
   * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
   * Entity must attached to an entity context.
   */
  @Generated(hash = 713229351)
  public void update() {
      if (myDao == null) {
          throw new DaoException("Entity is detached from DAO context");
      }
      myDao.update(this);
  }

  public Long getId() {
      return this.id;
  }


  public void setId(Long id) {
      this.id = id;
  }


/** called by internal mechanisms, do not call yourself. */
@Generated(hash = 2068258944)
public void __setDaoSession(DaoSession daoSession) {
    this.daoSession = daoSession;
    myDao = daoSession != null ? daoSession.getFormDao() : null;
}
}
