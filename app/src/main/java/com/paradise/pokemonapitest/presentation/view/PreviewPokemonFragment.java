package com.paradise.pokemonapitest.presentation.view;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.paradise.pokemonapitest.R;
import com.paradise.pokemonapitest.common.App;
import com.paradise.pokemonapitest.common.BundleCreator;
import com.paradise.pokemonapitest.common.Const;
import com.paradise.pokemonapitest.common.NavigatorHelper;
import com.paradise.pokemonapitest.data.pokemonPreview.PokemonPreview;
import com.paradise.pokemonapitest.pagination.adapter.PokemonAdapter;
import com.paradise.pokemonapitest.presentation.interfaces.PokemonClickListener;
import com.paradise.pokemonapitest.presentation.viewmodel.PreviewFragmentViewModel;
import com.paradise.pokemonapitest.presentation.viewmodel.ViewModelFactory;
import java.util.Objects;
import javax.inject.Inject;

public class PreviewPokemonFragment extends Fragment
    implements PokemonClickListener, LifecycleObserver {

  @Inject NavigatorHelper navHelper;
  @Inject ViewModelFactory viewModelFactory;
  static final String PREVIEW_FRAGMENT_STATE = "com.paradise.previewFragment_state";
  private PokemonAdapter adapter;
  private PreviewFragmentViewModel viewModel;
  private RecyclerView.LayoutManager layoutManager;
  private String state;
  private TextView errorText;
  private RecyclerView recyclerView;
  private SwipeRefreshLayout swipeRefreshLayout;
  private ActionBar actionBar;

  public PreviewPokemonFragment() {
  }

  static PreviewPokemonFragment getNewInstance() {
    return new PreviewPokemonFragment();
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    getLifecycle().addObserver(this);
    if (getArguments() != null) {
      state = getArguments().getString(PREVIEW_FRAGMENT_STATE);
      checkState(state);
    }
    viewModel = ViewModelProviders.of(this, viewModelFactory).get(PreviewFragmentViewModel.class);
  }

  private void checkState(String state) {
    if (state != null) {
      switch (state) {
        case Const.NETWORK_FRAGMENT:
          App.instance.getNetworkComponent().inject(this);
          break;
        case Const.LOCAL_FRAGMENT:
          App.instance.getLocalComponent().inject(this);
          break;
      }
    }
  }

  @Override public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_network_pokemon, container, false);
    initView(view);
    initSwipe(view);
    initRecyclerView(view);
    return view;
  }

  private void initView(View view) {
    actionBar = ((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar();
    errorText = view.findViewById(R.id.textView);
  }

  private void initSwipe(View view) {
    swipeRefreshLayout = view.findViewById(R.id.refresh_layout);
    if (state.equals(Const.NETWORK_FRAGMENT)) {
      swipeRefreshLayout.setOnRefreshListener(() -> viewModel.refreshLiveData());
    } else {
      swipeRefreshLayout.setEnabled(false);
    }
  }

  private void initRecyclerView(View view) {
    recyclerView = view.findViewById(R.id.pokemon_recycler);
    adapter = new PokemonAdapter(new DiffUtil.ItemCallback<PokemonPreview>() {
      @Override public boolean areItemsTheSame(@NonNull PokemonPreview oldItem,
          @NonNull PokemonPreview newItem) {
        return oldItem.getPokeName().equals(newItem.getPokeName());
      }

      @Override public boolean areContentsTheSame(@NonNull PokemonPreview oldItem,
          @NonNull PokemonPreview newItem) {
        return oldItem.getSprites()
            .getFrontDefault()
            .equals(newItem.getSprites().getFrontDefault());
      }
    });
    adapter.setPokemonClickListener(this);
    recyclerView.setAdapter(adapter);
    layoutManager = new GridLayoutManager(getContext(), 3, RecyclerView.VERTICAL, false);
    recyclerView.setLayoutManager(layoutManager);
  }

  @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    viewModel.getListLiveData().observe(getViewLifecycleOwner(), this::showPokemons);
    viewModel.getDataState().observe(getViewLifecycleOwner(), this::checkErrors);
    if (state.equals(Const.NETWORK_FRAGMENT)) {
      swipeRefreshLayout.setRefreshing(true);
      viewModel.getLoadingState().observe(getViewLifecycleOwner(), this::checkLoadState);
    }
  }

  private void showPokemons(PagedList<PokemonPreview> pokeList) {
    adapter.submitList(pokeList);
  }

  private void checkErrors(Boolean error) {
    if (error) {
      if (state.equals(Const.NETWORK_FRAGMENT)) {
        errorText.setText("Ошибка подключения. Потяните вниз для обновления");
      } else {
        errorText.setText("У вас нет сохраненных покемонов.");
      }
      recyclerView.setVisibility(View.GONE);
      errorText.setVisibility(View.VISIBLE);
    } else {
      errorText.setVisibility(View.GONE);
      recyclerView.setVisibility(View.VISIBLE);
    }
  }

  private void checkLoadState(Boolean isLoaded) {
    if (isLoaded && swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);
  }

  private void setRecyclerSpanNum(int i) {
    ((GridLayoutManager) layoutManager).setSpanCount(i);
  }

  @Override public void onPokemonClick(String pokemonName, int chainId) {
    navHelper.goToDetailFragmentWithBundle(BundleCreator.createBundle(chainId, pokemonName, state));
  }

  @OnLifecycleEvent(Lifecycle.Event.ON_RESUME) void onResumeActions() {
    if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
      setRecyclerSpanNum(4);
    } else {
      setRecyclerSpanNum(3);
    }
    if (state.equals(Const.LOCAL_FRAGMENT)) {
      viewModel.refreshLiveData();
    }
  }

  @OnLifecycleEvent(Lifecycle.Event.ON_START) void hideBackButton() {
    actionBar.setDisplayHomeAsUpEnabled(false);
  }

  @OnLifecycleEvent(Lifecycle.Event.ON_STOP) void showBackButton() {
    actionBar.setDisplayHomeAsUpEnabled(true);
  }

  @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY) void clear() {
    App.instance.clearLocalComponent();
    App.instance.clearNetworkComponent();
  }
}
