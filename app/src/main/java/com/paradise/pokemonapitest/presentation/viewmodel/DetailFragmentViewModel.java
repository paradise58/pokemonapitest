package com.paradise.pokemonapitest.presentation.viewmodel;

import androidx.lifecycle.MutableLiveData;
import com.paradise.pokemonapitest.data.Pokemon;
import com.paradise.pokemonapitest.domain.NetworkInteractor;
import io.reactivex.android.schedulers.AndroidSchedulers;
import javax.inject.Inject;

public class DetailFragmentViewModel extends DisposableViewModel {
  private NetworkInteractor networkInteractor;
  private MutableLiveData<Pokemon> pokemonLiveData;
  private MutableLiveData<Boolean> errorState;
  private MutableLiveData<Boolean> actionButtonLiveData;

  @Inject DetailFragmentViewModel(NetworkInteractor networkInteractor) {
    this.networkInteractor = networkInteractor;
    pokemonLiveData = new MutableLiveData<>();
    errorState = new MutableLiveData<>();
    actionButtonLiveData = new MutableLiveData<>();
  }
 public MutableLiveData<Boolean> getActionButtonLiveData (){
    return actionButtonLiveData;
  }
  public MutableLiveData<Boolean> getErrorState() {
    return errorState;
  }

  public MutableLiveData<Pokemon> getPokemonLiveData(int id, String name) {
    if (pokemonLiveData.getValue() == null) setPokemonLiveData(name, id);
    return pokemonLiveData;
  }

  private void setPokemonLiveData(String name, int chainId) {
    compositeDisposable.add(networkInteractor.getPokemon(name, chainId)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(s -> {

          pokemonLiveData.postValue(s);
        actionButtonLiveData.postValue(true);
          }, e -> errorState.postValue(true)));
  }

  public Boolean savePokemon() {
    return networkInteractor.saveToLocal(pokemonLiveData.getValue());
  }

  public void deletePokemon() {
    networkInteractor.deletePokemon(pokemonLiveData.getValue());
  }
}
