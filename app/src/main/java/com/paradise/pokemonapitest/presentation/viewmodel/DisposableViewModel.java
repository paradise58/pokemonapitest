package com.paradise.pokemonapitest.presentation.viewmodel;

import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;

public abstract class DisposableViewModel extends ViewModel {

  final CompositeDisposable compositeDisposable = new CompositeDisposable();

  @Override public void onCleared() {
    if (!compositeDisposable.isDisposed()) compositeDisposable.clear();
    super.onCleared();
  }
}

