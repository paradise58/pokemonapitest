package com.paradise.pokemonapitest.presentation.interfaces;

public interface PokemonClickListener {

  void onPokemonClick(String name, int id);
}
