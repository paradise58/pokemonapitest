package com.paradise.pokemonapitest.presentation.cicerone;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.paradise.pokemonapitest.presentation.view.ParentFragment;
import com.paradise.pokemonapitest.presentation.view.PokemonDetailFragment;
import ru.terrakok.cicerone.android.support.SupportAppScreen;

public class Screens {

  public static final class DetailScreen extends SupportAppScreen {
    Bundle bundle;

    public DetailScreen(Bundle bundle) {
      this.bundle = bundle;
    }

    @Override public Fragment getFragment() {
      return PokemonDetailFragment.getNewInstance(bundle);
    }
  }

  public static final class ParentScreen extends SupportAppScreen {
    @Override public Fragment getFragment() {
      return ParentFragment.getNewInstance();
    }
  }
}
