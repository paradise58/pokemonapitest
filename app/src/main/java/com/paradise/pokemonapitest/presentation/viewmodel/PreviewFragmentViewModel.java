package com.paradise.pokemonapitest.presentation.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import com.paradise.pokemonapitest.data.pokemonPreview.PokemonPreview;
import com.paradise.pokemonapitest.domain.NetworkInteractor;
import com.paradise.pokemonapitest.pagination.FeedDataFactory;
import java.util.concurrent.Executors;
import javax.inject.Inject;

public class PreviewFragmentViewModel extends ViewModel {

  private PagedList.Config config;
  private MutableLiveData<Boolean> isEmpty = new MutableLiveData<>();
  private FeedDataFactory factory;
  private LiveData<PagedList<PokemonPreview>> listLiveData;
  private MutableLiveData<Boolean> isLoaded = new MutableLiveData<>();

  @Inject PreviewFragmentViewModel(NetworkInteractor networkInteractor) {
    config = new PagedList.Config.Builder().setEnablePlaceholders(false).setPageSize(24).build();
    factory = new FeedDataFactory(networkInteractor) {
    };
 createLivePagedList();
  }

  public void refreshLiveData() {
    isEmpty.postValue(false);
    isLoaded.postValue(false);
    if (factory.getPositionalPokemonDataSource() != null) factory.getPositionalPokemonDataSource().invalidate();
  }

  private void createLivePagedList() {
    listLiveData = new LivePagedListBuilder<>(factory, config).setFetchExecutor(
        Executors.newFixedThreadPool(5))
        .setBoundaryCallback(new PagedList.BoundaryCallback<PokemonPreview>() {
          @Override public void onItemAtFrontLoaded(@NonNull PokemonPreview itemAtFront) {
            super.onItemAtFrontLoaded(itemAtFront);
            isLoaded.postValue(true);
          }

          @Override public void onZeroItemsLoaded() {
            isEmpty.postValue(true);
            isLoaded.postValue(true);
          }
        })
        .build();
  }

  public LiveData<PagedList<PokemonPreview>> getListLiveData() {
    return listLiveData;
  }

  public MutableLiveData<Boolean> getDataState() {
    return isEmpty;
  }

  public MutableLiveData<Boolean> getLoadingState() {
    return isLoaded;
  }

  @Override protected void onCleared() {
    factory.getPositionalPokemonDataSource().clear();
    super.onCleared();
  }
}
