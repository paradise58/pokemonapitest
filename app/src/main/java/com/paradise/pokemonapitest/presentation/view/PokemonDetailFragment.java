package com.paradise.pokemonapitest.presentation.view;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import com.paradise.pokemonapitest.R;
import com.paradise.pokemonapitest.common.App;
import com.paradise.pokemonapitest.common.BundleCreator;
import com.paradise.pokemonapitest.common.Const;
import com.paradise.pokemonapitest.common.GlideApp;
import com.paradise.pokemonapitest.common.NavigatorHelper;
import com.paradise.pokemonapitest.common.StringHelper;
import com.paradise.pokemonapitest.data.Form;
import com.paradise.pokemonapitest.data.Pokemon;
import com.paradise.pokemonapitest.data.Skill;
import com.paradise.pokemonapitest.domain.NetworkInteractor;
import com.paradise.pokemonapitest.presentation.viewmodel.DetailFragmentViewModel;
import com.paradise.pokemonapitest.presentation.viewmodel.ViewModelFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class PokemonDetailFragment extends Fragment {

  @Inject NavigatorHelper navigatorHelper;
  @Inject NetworkInteractor networkInteractor;
  @Inject ViewModelFactory viewModelFactory;
  public static final String POKEMON_NAME = "Pokemon Name";
  public static final String POKEMON_CHAIN_ID = "Pokemon ChainId";
  public static final String DETAIL_FRAGMENT_STATE = "com.paradise.detailFragment_state";
  private String state;
  private MenuItem actionSave, actionDelete;
  private ImageView pokemonImage, attackImg, defenceImg, speedImg, hpImg;
  private TextView name, weight, type, hp, defence, attack, speed, label;
  private LinearLayout linearLayout, nextFormLayout;
  private DetailFragmentViewModel viewModel;
  private ProgressBar progressBar;

  public PokemonDetailFragment() {
  }

  public static PokemonDetailFragment getNewInstance(Bundle bundle) {
    PokemonDetailFragment fragment = new PokemonDetailFragment();
    fragment.setArguments(bundle);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    injectComponent();
    viewModel = ViewModelProviders.of(this, viewModelFactory).get(DetailFragmentViewModel.class);
  }

  private void injectComponent() {
    if (getArguments() != null) {
      state = getArguments().getString(DETAIL_FRAGMENT_STATE);
      if (state != null) {
        switch (state) {
          case Const.NETWORK_FRAGMENT:
            App.instance.getNetworkComponent().inject(this);
            break;
          case Const.LOCAL_FRAGMENT:
            App.instance.getLocalComponent().inject(this);
            break;
        }
      }
    }
  }

  @Override public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_pokemon_detail, container, false);
    initView(view);
    return view;
  }

  @Override public void onPrepareOptionsMenu(Menu menu) {
    actionSave = menu.findItem(R.id.action_save);
    actionDelete = menu.findItem(R.id.action_delete);
    viewModel.getActionButtonLiveData().observe(getViewLifecycleOwner(), this::showActionButton);
  }

  private void showActionButton(Boolean visible) {
    if (visible) {
      if (state.equals(Const.NETWORK_FRAGMENT)) {
        actionSave.setVisible(true);
      } else if (state.equals(Const.LOCAL_FRAGMENT)) {
        Log.d(TAG, "showActionButton: ");
        actionDelete.setVisible(true);
      }
    }
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    int itemId = item.getItemId();
    switch (itemId) {
      case R.id.action_save:
        if (viewModel.savePokemon()) {
          Toast.makeText(requireContext(), "Покемон сохранён.", Toast.LENGTH_SHORT).show();
        } else {
          Toast.makeText(requireContext(), "Такой покемон уже есть.", Toast.LENGTH_SHORT).show();
        }
        return true;
      case R.id.action_delete:
        viewModel.deletePokemon();
        navigatorHelper.exit();
        Toast.makeText(requireContext(), "Покемон удалён.", Toast.LENGTH_SHORT).show();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    if (getArguments() != null) {
      int id = getArguments().getInt(POKEMON_CHAIN_ID);
      String name = getArguments().getString(POKEMON_NAME);
      viewModel.getPokemonLiveData(id, name).observe(getViewLifecycleOwner(), this::setupViewData);
      viewModel.getErrorState().observe(getViewLifecycleOwner(), this::checkErrorState);
      if (state.equals(Const.NETWORK_FRAGMENT)) progressBar.setVisibility(View.VISIBLE);
    }
  }

  private void checkErrorState(Boolean error) {
    if (error && state.equals(Const.NETWORK_FRAGMENT)) {
      Toast.makeText(requireContext(), "Ошибка. Проверьте наличие интернета.", Toast.LENGTH_SHORT)
          .show();
    } else if (error) {
      Toast.makeText(requireContext(), "У вас нет такого покемона.", Toast.LENGTH_SHORT).show();
    }
    navigatorHelper.exit();
  }

  private void initView(View view) {
    name = view.findViewById(R.id.detail_name_text);
    type = view.findViewById(R.id.detail_type_text);
    weight = view.findViewById(R.id.detail_weight_text);
    label = view.findViewById(R.id.next_form_label);

    hp = view.findViewById(R.id.detail_hp_text);
    defence = view.findViewById(R.id.detail_defence_text);
    speed = view.findViewById(R.id.detail_speed_text);
    attack = view.findViewById(R.id.detail_attack_text);

    pokemonImage = view.findViewById(R.id.detail_pokemon_image);
    attackImg = view.findViewById(R.id.detail_attack_icon);
    defenceImg = view.findViewById(R.id.detail_defence_icon);
    hpImg = view.findViewById(R.id.detail_hp_icon);
    speedImg = view.findViewById(R.id.detail_speed_icon);

    linearLayout = view.findViewById(R.id.ability_container);
    nextFormLayout = view.findViewById(R.id.next_form_container);

    progressBar = view.findViewById(R.id.my_progress_bar);
  }

  private void setupViewData(Pokemon pokemon) {
    clearLayouts();
    GlideApp.with(requireContext())
        .load(pokemon.getSprites().getFrontDefault())
        .thumbnail(0.1f)
        .into(pokemonImage);
    attackImg.setImageResource(R.drawable.sword_icon);
    speedImg.setImageResource(R.drawable.speed_icon);
    hpImg.setImageResource(R.drawable.pokemon_hp);
    defenceImg.setImageResource(R.drawable.defence_icon);
    setPokemonAbility(pokemon.getSkills());
    name.setText(StringHelper.firstCharToUpperCase(pokemon.getPokeName()));
    weight.setText(new StringBuilder("Weight : " + String.valueOf(pokemon.getWeight())));
    type.setText(setType(pokemon.getTypeList()));
    label.setText(getString(R.string.next_form_label));

    hp.setText(String.valueOf(pokemon.getHp()));
    speed.setText(String.valueOf(pokemon.getSpeed()));
    attack.setText(String.valueOf(pokemon.getAttack()));
    defence.setText(String.valueOf(pokemon.getDefence()));
    setNextForm(pokemon.getForms(), pokemon.getPokeName(), pokemon.getChainId());

    progressBar.setVisibility(View.INVISIBLE);
  }

  private void clearLayouts() {
    linearLayout.removeAllViewsInLayout();
    nextFormLayout.removeAllViewsInLayout();
  }

  private void setNextForm(List<Form> forms, String pokeName, int chainId) {
    List<String> fstGen = new ArrayList<>();
    List<String> scndGen = new ArrayList<>();
    List<String> trdGen = new ArrayList<>();
    for (Form form : forms) {
      if (form.getGen() == 1) {
        fstGen.add(form.getFormName());
      } else if (form.getGen() == 2) {
        scndGen.add(form.getFormName());
      } else if (form.getGen() == 3) trdGen.add(form.getFormName());
    }
    if (fstGen.contains(pokeName) && !scndGen.isEmpty()) {
      setupNextForm(scndGen, chainId);
    } else if (scndGen.contains(pokeName) && !trdGen.isEmpty()) {
      setupNextForm(trdGen, chainId);
    } else {
      createNextFormView();
    }
  }

  private void setupNextForm(List<String> formsNames, int chainId) {
    for (String formName : formsNames) {
      createNextFormView(formName, chainId);
    }
  }

  private void createNextFormView() {
    TextView textView = createEmptyViewForm();
    textView.setText(getString(R.string.nextForm));
  }

  private void createNextFormView(String formName, int evolutionChainId) {
    TextView textView = createEmptyViewForm();
    textView.setText(formName);
    textView.setText(StringHelper.firstCharToUpperCase(formName));
    textView.setTextColor(getResources().getColor(R.color.linkColor));
    textView.setOnClickListener(v -> navigatorHelper.goToDetailFragmentWithBundle(
        BundleCreator.createBundle(evolutionChainId, formName, state)));
    textView.setClickable(true);
  }

  private TextView createEmptyViewForm() {
    TextView textView = new TextView(requireContext());
    LinearLayout.LayoutParams layoutParams =
        new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT);
    layoutParams.weight = 1.0f;
    layoutParams.gravity = Gravity.CENTER;
    textView.setGravity(Gravity.CENTER);
    textView.setLayoutParams(layoutParams);
    nextFormLayout.addView(textView);
    return textView;
  }

  private String setType(List<String> types) {
    StringBuilder sb = new StringBuilder("Type : ");
    for (int i = 0; i < types.size(); i++) {
      sb.append(types.get(i));
      if (i < types.size() - 1) sb.append(", ");
    }
    sb.append(".");
    return sb.toString();
  }

  private void setPokemonAbility(List<Skill> skills) {
    int i = 1;
    Collections.sort(skills, (o1, o2) -> o1.getName().compareTo(o2.getName()));
    for (Skill skill : skills) {
      String abilityName =
          String.valueOf(i++) + ") Ability : " + StringHelper.firstCharToUpperCase(skill.getName());

      linearLayout.addView(createAbilityDescriptionView(abilityName));
      linearLayout.addView(createAbilityDescriptionView(skill.getFlavorTextEntries()));
      linearLayout.addView(createAbilityDescriptionView(skill.getEffectEntries()));
    }
  }

  private TextView createAbilityDescriptionView(String string) {
    TextView textView = new TextView(requireContext());
    textView.setText(string);
    textView.setTextColor(Color.BLACK);
    textView.setPadding(0, 8, 0, 0);
    return textView;
  }
}
