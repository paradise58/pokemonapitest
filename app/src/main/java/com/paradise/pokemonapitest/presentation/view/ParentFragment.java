package com.paradise.pokemonapitest.presentation.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.paradise.pokemonapitest.R;
import com.paradise.pokemonapitest.common.Const;

public class ParentFragment extends Fragment {

  public ParentFragment() {
  }

  public static ParentFragment getNewInstance() {
    return new ParentFragment();
  }

  @Override public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_parent, container, false);
    ViewPager viewPager = view.findViewById(R.id.view_pager);
    viewPager.setAdapter(createViewPagerAdapter());
    return view;
  }

  private PagerAdapter createViewPagerAdapter() {
    return new FragmentPagerAdapter(getChildFragmentManager()) {
      @Override public Fragment getItem(int position) {
        PreviewPokemonFragment fragment = PreviewPokemonFragment.getNewInstance();
        switch (position) {
          case 0:
            fragment.setArguments(createBundle(Const.NETWORK_FRAGMENT));
            return fragment;
          case 1:
            fragment.setArguments(createBundle(Const.LOCAL_FRAGMENT));
            return fragment;
          default:
            return new ErrorFragment();
        }
      }

      @Override public int getCount() {
        return 2;
      }

      @Override public CharSequence getPageTitle(int position) {
        switch (position) {
          case 0:
            return "Network";
          case 1:
            return "Local";
          default:
            return "PageAdapterError";
        }
      }
    };
  }

  private Bundle createBundle(String state) {
    Bundle bundle = new Bundle();
    bundle.putString(PreviewPokemonFragment.PREVIEW_FRAGMENT_STATE, state);
    return bundle;
  }
}
