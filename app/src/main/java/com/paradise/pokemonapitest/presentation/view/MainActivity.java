package com.paradise.pokemonapitest.presentation.view;

import android.os.Bundle;
import android.view.Menu;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import com.paradise.pokemonapitest.R;
import com.paradise.pokemonapitest.common.App;
import com.paradise.pokemonapitest.presentation.cicerone.Screens;
import java.util.Objects;
import javax.inject.Inject;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Replace;

public class MainActivity extends AppCompatActivity {
  @Inject NavigatorHolder navigatorHolder;

  private Navigator navigator = new SupportAppNavigator(this, R.id.container) {
    @Override public void applyCommands(Command[] commands) {
      super.applyCommands(commands);
    }
  };

  @Override protected void onCreate(Bundle savedInstanceState) {
    App.instance.getAppComponent().inject(this);
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    initToolBar();
    navigatorHolder.setNavigator(navigator);
    if (savedInstanceState == null) {
      navigator.applyCommands(new Command[] { new Replace(new Screens.ParentScreen()) });
    }
  }

  private void initToolBar() {
    Toolbar toolbar = findViewById(R.id.tool_bar);
    setSupportActionBar(toolbar);
    Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    toolbar.setNavigationOnClickListener(v -> onBackPressed());
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.tool_menu, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override public void onBackPressed() {
    if (isLastScreen()) {
      showExitDialog();
    } else {
      super.onBackPressed();
    }
  }

  private boolean isLastScreen() {
    return getSupportFragmentManager().getBackStackEntryCount() == 0;
  }

  private void showExitDialog() {
    new AlertDialog.Builder(this).setTitle("Действительно хотите выйти ?")
        .setPositiveButton("Выйти", (d, i) -> finish())
        .setNegativeButton("Остаться", (d, i) -> d.dismiss())
        .create()
        .show();
  }
}
