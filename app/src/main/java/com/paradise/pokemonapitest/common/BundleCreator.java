package com.paradise.pokemonapitest.common;

import android.os.Bundle;
import com.paradise.pokemonapitest.presentation.view.PokemonDetailFragment;

public class BundleCreator {

  public static Bundle createBundle(int chainId, String pokemonName, String state) {
    Bundle bundle = new Bundle();
    bundle.putInt(PokemonDetailFragment.POKEMON_CHAIN_ID, chainId);
    bundle.putString(PokemonDetailFragment.POKEMON_NAME, pokemonName);
    bundle.putString(PokemonDetailFragment.DETAIL_FRAGMENT_STATE, state);
    return bundle;
  }
}
