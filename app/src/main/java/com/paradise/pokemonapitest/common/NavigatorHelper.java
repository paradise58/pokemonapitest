package com.paradise.pokemonapitest.common;

import android.os.Bundle;
import com.paradise.pokemonapitest.presentation.cicerone.Screens;
import ru.terrakok.cicerone.Router;

public class NavigatorHelper {
  private Router router;

  public NavigatorHelper(Router router) {
    this.router = router;
  }

  public void goToDetailFragmentWithBundle(Bundle bundle) {
    router.navigateTo(new Screens.DetailScreen(bundle));
  }

  public void exit() {
    router.exit();
  }
}
