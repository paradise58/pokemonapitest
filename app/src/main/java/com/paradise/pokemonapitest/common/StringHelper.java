package com.paradise.pokemonapitest.common;

public class StringHelper {
  public static String firstCharToUpperCase(String string) {
    StringBuilder stringBuilder = new StringBuilder(string);
    stringBuilder.setCharAt(0, Character.toUpperCase(stringBuilder.charAt(0)));
    return stringBuilder.toString();
  }

  public static int convertToId(String str) {
    return Integer.parseInt(
        str.replace("https://pokeapi.co/api/v2/evolution-chain/", "").replace("/", ""));
  }
}
