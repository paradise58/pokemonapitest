package com.paradise.pokemonapitest.common;

import java.util.Arrays;
import java.util.List;
import org.greenrobot.greendao.converter.PropertyConverter;

public class GreenConverter implements PropertyConverter<List<String>, String> {
  @Override public List<String> convertToEntityProperty(String databaseValue) {
    if (databaseValue == null) {
      return null;
    } else {
      return Arrays.asList(databaseValue.split(","));
    }
  }

  @Override public String convertToDatabaseValue(List<String> entityProperty) {
    if (entityProperty == null) {
      return null;
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      for (String str : entityProperty) {
        stringBuilder.append(str);
        stringBuilder.append(",");
      }
      return stringBuilder.toString();
    }
  }
}
