package com.paradise.pokemonapitest.common;

import androidx.multidex.MultiDexApplication;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.paradise.pokemonapitest.dagger.component.AppComponent;
import com.paradise.pokemonapitest.dagger.component.DaggerAppComponent;
import com.paradise.pokemonapitest.dagger.component.LocalComponent;
import com.paradise.pokemonapitest.dagger.component.NetworkComponent;
import com.paradise.pokemonapitest.data.DaoMaster;
import com.paradise.pokemonapitest.data.DaoSession;
import com.paradise.pokemonapitest.repositories.Retrofit.RetrofitAdapter;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import org.greenrobot.greendao.database.Database;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends MultiDexApplication {
  RetrofitAdapter adapter;
  public static App instance;
  private AppComponent appComponent;
  private LocalComponent localComponent;
  private NetworkComponent networkComponent;
  DaoSession daoSession;

  @Override public void onCreate() {
    super.onCreate();
    instance = this;
    initStetho();
    initDB();
  }

  private void initDB() {
    DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "pokemon_db");
    Database db = helper.getWritableDb();
    daoSession = new DaoMaster(db).newSession();
  }

  public DaoSession getDaoSession() {
    return daoSession;
  }

  private void initStetho() {
    Stetho.InitializerBuilder initializerBuilder = Stetho.newInitializerBuilder(this);
    initializerBuilder.enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this));
    initializerBuilder.enableDumpapp(Stetho.defaultDumperPluginsProvider(this));
    Stetho.Initializer initializer = initializerBuilder.build();
    Stetho.initialize(initializer);
  }

  public RetrofitAdapter getRetrofitAdapter() {
    Retrofit retrofit = new Retrofit.Builder().baseUrl("https://pokeapi.co/api/v2/")
        .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
        .addConverterFactory(GsonConverterFactory.create())
        .client(getHttpClient())
        .build();
    adapter = retrofit.create(RetrofitAdapter.class);
    return adapter;
  }

  private OkHttpClient getHttpClient() {
    return new OkHttpClient.Builder().addNetworkInterceptor(new StethoInterceptor()).build();
  }

  public AppComponent getAppComponent() {
    if (appComponent == null) appComponent = DaggerAppComponent.builder().build();
    return appComponent;
  }

  public NetworkComponent getNetworkComponent() {
    if (networkComponent == null) networkComponent = appComponent.networkComponent();
    return networkComponent;
  }

  public LocalComponent getLocalComponent() {
    if (localComponent == null) localComponent = appComponent.localComponent();
    return localComponent;
  }

  public void clearLocalComponent() {
    localComponent = null;
  }

  public void clearNetworkComponent() {
    networkComponent = null;
  }
}
