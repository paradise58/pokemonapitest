package com.paradise.pokemonapitest.common;

public interface Const {

  String NETWORK_FRAGMENT = "com.paradise.Network";
  String LOCAL_FRAGMENT = "com.paradise.Local";
}
