package com.paradise.pokemonapitest.repositories;

import com.paradise.pokemonapitest.common.StringHelper;
import com.paradise.pokemonapitest.data.Form;
import com.paradise.pokemonapitest.data.Pokemon;
import com.paradise.pokemonapitest.data.Skill;
import com.paradise.pokemonapitest.data.pokemonInfo.AbilitySource.AbilitySource;
import com.paradise.pokemonapitest.data.pokemonInfo.PokemonInfoSource.PokemonInfoSource;
import com.paradise.pokemonapitest.data.pokemonInfo.PokemonInfoSource.Type;
import com.paradise.pokemonapitest.data.pokemonPreview.EvolutionChainSource.EvolutionChainSource;
import com.paradise.pokemonapitest.data.pokemonPreview.EvolutionChainSource.EvolvesTo;
import com.paradise.pokemonapitest.data.pokemonPreview.EvolutionChainSource.EvolvesTo_;
import com.paradise.pokemonapitest.data.pokemonPreview.FormSource.FormSource;
import com.paradise.pokemonapitest.data.pokemonPreview.PokemonPreview;
import com.paradise.pokemonapitest.repositories.Retrofit.RetrofitAdapter;
import io.reactivex.Observable;
import io.reactivex.Single;
import java.util.ArrayList;
import java.util.List;

public class NetworkRepo implements PokemonRepository {

  private RetrofitAdapter retrofitAdapter;

  public NetworkRepo(RetrofitAdapter retrofitAdapter) {
    this.retrofitAdapter = retrofitAdapter;
  }

  @Override public Observable<Pokemon> getPokemon(String name, int chainId) {
    return retrofitAdapter.downloadPokemonSource(name).flatMap(pokemonInfoSource -> {
      List<AbilitySource> abilitySourceList = new ArrayList<>();
      return retrofitAdapter.downloadEvolutionChainSource(chainId)
          .flatMap(evolutionChainSource -> Observable.fromIterable(pokemonInfoSource.getAbilities())
              .flatMap(
                  ability -> retrofitAdapter.downloadAbilitySource(ability.getAbility().getName())
                      .map(abilitySourceList::add))
              .takeLast(1)
              .map(
                  s -> setPokemonData(pokemonInfoSource, abilitySourceList, evolutionChainSource)));
    });
  }

  private Pokemon setPokemonData(PokemonInfoSource pokemonInfoSource,
      List<AbilitySource> abilitySource, EvolutionChainSource evolutionChainSource) {
    List<Skill> skills = new ArrayList<>();
    List<String> skillsName = new ArrayList<>();
    List<String> typeList = new ArrayList<>();
    for (Type type : pokemonInfoSource.getTypes()) {
      typeList.add(type.getType().getName());
    }
    List<Form> forms = new ArrayList<>();
    String fstFormName = evolutionChainSource.getChain().getSpecies().getName();
    forms.add(new Form(1, fstFormName));
    if (evolutionChainSource.getChain().getEvolvesTo() != null
        && evolutionChainSource.getChain().getEvolvesTo().size() > 0) {
      for (EvolvesTo evolv2 : evolutionChainSource.getChain().getEvolvesTo()) {
        String scdFormName = evolv2.getSpecies().getName();
        forms.add(new Form(2, scdFormName));
        if (evolv2.getEvolvesTo() != null && evolv2.getEvolvesTo().size() > 0) {
          for (EvolvesTo_ evolv3 : evolv2.getEvolvesTo()) {
            String trdFormName = evolv3.getSpecies().getName();
            forms.add(new Form(3, trdFormName));
          }
        }
      }
    }
    for (AbilitySource ability : abilitySource) {
      Skill skill = new Skill();
      skill.setEffectEntries(ability.getEffectEntries().get(0).getEffect());
      skill.setFlavorTextEntries(ability.getFlavorTextEntries().get(2).getFlavorText());
      skill.setName(ability.getName());
      skillsName.add(ability.getName());
      skills.add(skill);
    }

    Pokemon pokemon = new Pokemon();
    pokemon.setSkillNameList(skillsName);
    pokemon.setPokeName(pokemonInfoSource.getName());
    pokemon.setWeight(pokemonInfoSource.getWeight());
    pokemon.setDefence(pokemonInfoSource.getStats().get(3).getBaseStat());
    pokemon.setSpeed(pokemonInfoSource.getStats().get(0).getBaseStat());
    pokemon.setAttack(pokemonInfoSource.getStats().get(4).getBaseStat());
    pokemon.setHp(pokemonInfoSource.getStats().get(5).getBaseStat());
    pokemon.setChainId(evolutionChainSource.getId());
    pokemonInfoSource.getSprites().setPokeName(pokemonInfoSource.getName());
    pokemon.setSprites(pokemonInfoSource.getSprites());
    pokemon.setSkills(skills);

    pokemon.setForms(forms);

    pokemon.setTypeList(typeList);
    return pokemon;
  }

  @Override
  public Single<List<PokemonPreview>> getPokemonsPreview(int startPosition, int loadSize) {
    return retrofitAdapter.getEvolutionChainPage(startPosition, loadSize)
        .onErrorResumeNext(Observable.empty())
        .flatMap(s -> Observable.fromIterable(s.getResults()))
        .flatMap(
            s -> retrofitAdapter.downloadEvolutionChainSource(StringHelper.convertToId(s.getUrl()))
                .onErrorResumeNext(Observable.empty())
                .flatMap(evolutionChainSource -> retrofitAdapter.downloadFormSource(
                    evolutionChainSource.getChain().getSpecies().getName())
                    .onErrorResumeNext(Observable.empty())
                    .filter(ds -> ds.getSprites().getFrontDefault() != null)
                    .map(formSource -> setPreviewData(formSource, evolutionChainSource))))
        .toSortedList((o1, o2) -> {
          if (o1.getChainId() == o2.getChainId()) {
            return 0;
          }
          return o1.getChainId() > o2.getChainId() ? 1 : -1;
        });
  }

  private PokemonPreview setPreviewData(FormSource formSource,
      EvolutionChainSource evolutionChainSource) {
    PokemonPreview pokemonPreview = new PokemonPreview();
    pokemonPreview.setChainId(evolutionChainSource.getId());
    pokemonPreview.setName(evolutionChainSource.getChain().getSpecies().getName());
    pokemonPreview.setSprites(formSource.getSprites());
    return pokemonPreview;
  }
}
