package com.paradise.pokemonapitest.repositories;

import com.paradise.pokemonapitest.common.App;
import com.paradise.pokemonapitest.data.DaoSession;
import com.paradise.pokemonapitest.data.Form;
import com.paradise.pokemonapitest.data.FormDao;
import com.paradise.pokemonapitest.data.Pokemon;
import com.paradise.pokemonapitest.data.PokemonDao;
import com.paradise.pokemonapitest.data.Skill;
import com.paradise.pokemonapitest.data.SkillDao;
import com.paradise.pokemonapitest.data.pokemonPreview.FormSource.Sprites;
import com.paradise.pokemonapitest.data.pokemonPreview.FormSource.SpritesDao;
import com.paradise.pokemonapitest.data.pokemonPreview.PokemonPreview;
import io.reactivex.Observable;
import io.reactivex.Single;
import java.util.ArrayList;
import java.util.List;

public class LocalRepo implements PokemonRepository, LocalRepoMethods {
  private SkillDao skillDao;
  private PokemonDao pokemonDao;
  private FormDao formDao;
  private SpritesDao spritesDao;

  public LocalRepo() {
    DaoSession daoSession = App.instance.getDaoSession();
    skillDao = daoSession.getSkillDao();
    pokemonDao = daoSession.getPokemonDao();
    formDao = daoSession.getFormDao();
    spritesDao = daoSession.getSpritesDao();
  }

  @Override public Single<List<PokemonPreview>> getPokemonsPreview(int start, int loadSize) {
    List<Pokemon> list = pokemonDao.queryBuilder()
        .where(PokemonDao.Properties.ChainId.isNotNull())
        .offset(start)
        .limit(loadSize)
        .list();
    return Observable.fromIterable(list).cast(PokemonPreview.class).toList();
  }

  @Override public Observable<Pokemon> getPokemon(String name, int chainId) {
    if (pokemonDao.load(name) != null) {
      Pokemon pokemon = pokemonDao.load(name);
      List<Skill> skillList = new ArrayList<>();
      List<String> skillNameList = pokemon.getSkillNameList();
      for (String skillName : skillNameList) {
        Skill skill = skillDao.load(skillName);
        skillList.add(skill);
        pokemon.setSkills(skillList);
      }
      return Observable.just(pokemonDao.load(name));
    } else {
      return Observable.error(new Exception());
    }
  }

  @Override public Boolean saveToLocal(Pokemon pokemon) {
    if (pokemon != null && pokemonDao.load(pokemon.getPokeName()) == null) {
      pokemonDao.insert(pokemon);
      for (Skill skill : pokemon.getSkills()) {
        Skill load = skillDao.load(skill.getName());
        if (load == null) {
          skillDao.insert(skill);
        } else {
          skill.setUsers(load.getUsers() + 1);
          skillDao.update(skill);
        }
      }

      if (formDao.queryBuilder()
          .where(FormDao.Properties.ChainId.eq(pokemon.getChainId()))
          .build()
          .list()
          .size() == 0) {
        for (Form form : pokemon.getForms()) {
          form.setChainId(pokemon.getChainId());
          formDao.insert(form);
        }
      }
      spritesDao.insert(pokemon.getSprites());
      return true;
    }
    return false;
  }

  @Override public void deletePokemon(Pokemon pokemon) {
    if (pokemonDao.load(pokemon.getPokeName()) != null) {
      deleteSprites(pokemon.getSprites());
      if (pokemonDao.queryBuilder()
          .where(PokemonDao.Properties.ChainId.eq(pokemon.getChainId()))
          .build()
          .list()
          .size() == 1) {
        formDao.queryBuilder()
            .where(FormDao.Properties.ChainId.eq(pokemon.getChainId()))
            .buildDelete()
            .executeDeleteWithoutDetachingEntities();
      }
      for (Skill pokeSkill : pokemon.getSkills()) {
        Skill loadedSkill = skillDao.load(pokeSkill.getName());
        if (loadedSkill.getUsers() == 1) {
          skillDao.delete(pokeSkill);
        } else {
          loadedSkill.setUsers(loadedSkill.getUsers() - 1);
          skillDao.update(loadedSkill);
        }
      }
      pokemonDao.delete(pokemon);
    }
  }

  private void deleteSprites(Sprites sprites) {
    spritesDao.delete(sprites);
  }
}
