package com.paradise.pokemonapitest.repositories.Retrofit;

import com.paradise.pokemonapitest.data.pokemonInfo.AbilitySource.AbilitySource;
import com.paradise.pokemonapitest.data.pokemonInfo.PokemonInfoSource.PokemonInfoSource;
import com.paradise.pokemonapitest.data.pokemonPreview.EvChain;
import com.paradise.pokemonapitest.data.pokemonPreview.EvolutionChainSource.EvolutionChainSource;
import com.paradise.pokemonapitest.data.pokemonPreview.FormSource.FormSource;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitAdapter {

  @GET("pokemon/{name}/") Observable<PokemonInfoSource> downloadPokemonSource(
      @Path("name") String name);

  @GET("evolution-chain/{id}/") Observable<EvolutionChainSource> downloadEvolutionChainSource(
      @Path("id") int pokemonId);

  @GET("ability/{name}/") Observable<AbilitySource> downloadAbilitySource(
      @Path("name") String ability);

  @GET("pokemon-form/{name}/") Observable<FormSource> downloadFormSource(
      @Path("name") String pokemonName);

  @GET("evolution-chain/") Observable<EvChain> getEvolutionChainPage(@Query("offset") int offset,
      @Query("limit") int limit);
}
