package com.paradise.pokemonapitest.repositories;

import com.paradise.pokemonapitest.data.Pokemon;

public interface LocalRepoMethods {

  Boolean saveToLocal(Pokemon pokemon);

  void deletePokemon(Pokemon pokemon);
}
