package com.paradise.pokemonapitest.repositories;

import com.paradise.pokemonapitest.data.Pokemon;
import com.paradise.pokemonapitest.data.pokemonPreview.PokemonPreview;
import io.reactivex.Observable;
import io.reactivex.Single;
import java.util.List;

public interface PokemonRepository {

  Single<List<PokemonPreview>> getPokemonsPreview(int startPosition, int loadSize);

  Observable<Pokemon> getPokemon(String name, int chainId);
}
