package com.paradise.pokemonapitest.domain;

import com.paradise.pokemonapitest.data.Pokemon;
import com.paradise.pokemonapitest.data.pokemonPreview.PokemonPreview;
import com.paradise.pokemonapitest.repositories.LocalRepoMethods;
import com.paradise.pokemonapitest.repositories.PokemonRepository;
import io.reactivex.Observable;
import io.reactivex.Single;
import java.util.List;

public class NetworkInteractor {
  private PokemonRepository repository;
  private LocalRepoMethods localRepoMethods;

  public NetworkInteractor(PokemonRepository pokemonRepository, LocalRepoMethods localRepoMethods) {
    repository = pokemonRepository;
    this.localRepoMethods = localRepoMethods;
  }

  public Single<List<PokemonPreview>> getSortedPokemonPreviewList(int startPosition, int loadSize) {
    return repository.getPokemonsPreview(startPosition, loadSize);
  }

  public Observable<Pokemon> getPokemon(String name, int chainId) {
    return repository.getPokemon(name, chainId);
  }

  public Boolean saveToLocal(Pokemon pokemon) {
    return localRepoMethods.saveToLocal(pokemon);
  }

  public void deletePokemon(Pokemon pokemon) {
    localRepoMethods.deletePokemon(pokemon);
  }
}

