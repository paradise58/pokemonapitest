package com.paradise.pokemonapitest.pagination.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.paradise.pokemonapitest.R;
import com.paradise.pokemonapitest.common.GlideApp;
import com.paradise.pokemonapitest.data.pokemonPreview.PokemonPreview;
import com.paradise.pokemonapitest.presentation.interfaces.PokemonClickListener;
import java.util.Objects;

public class PokemonAdapter
    extends PagedListAdapter<PokemonPreview, PokemonAdapter.PokemonViewHolder> {
  private PokemonClickListener pokemonClickListener;

  public PokemonAdapter(@NonNull DiffUtil.ItemCallback<PokemonPreview> diffCallback) {
    super(diffCallback);
  }

  public void setPokemonClickListener(PokemonClickListener pokemonClickListener) {
    this.pokemonClickListener = pokemonClickListener;
  }

  @NonNull @Override
  public PokemonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.poke_card, parent, false);
    PokemonViewHolder viewHolder = new PokemonViewHolder(view);
    view.setOnClickListener(viewHolder);
    return viewHolder;
  }

  @Override public void onBindViewHolder(@NonNull PokemonViewHolder holder, int position) {
    holder.bindView(getItem(position));
  }

  class PokemonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    ImageView image;
    TextView name;

    PokemonViewHolder(@NonNull View itemView) {
      super(itemView);
      image = itemView.findViewById(R.id.image);
      name = itemView.findViewById(R.id.name);
    }

    void bindView(PokemonPreview pokemonPreview) {
      GlideApp.with(name.getContext())
          .load(pokemonPreview.getSprites().getFrontDefault())
          .into(image);
      StringBuilder sb = new StringBuilder(pokemonPreview.getPokeName());
      sb.setCharAt(0, Character.toUpperCase(pokemonPreview.getPokeName().charAt(0)));
      name.setText(sb.toString());
    }

    @Override public void onClick(View v) {
      String pokemonName = Objects.requireNonNull(getItem(getAdapterPosition())).getPokeName();
      int evolutionChainId = Objects.requireNonNull(getItem(getAdapterPosition())).getChainId();
      pokemonClickListener.onPokemonClick(pokemonName, evolutionChainId);
    }
  }
}
