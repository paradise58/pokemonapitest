package com.paradise.pokemonapitest.pagination;

import androidx.paging.DataSource;
import com.paradise.pokemonapitest.data.pokemonPreview.PokemonPreview;
import com.paradise.pokemonapitest.domain.NetworkInteractor;

public class FeedDataFactory extends DataSource.Factory<Integer, PokemonPreview> {

  private PositionalPokemonDataSource positionalPokemonDataSource;
  private NetworkInteractor networkInteractor;

  protected FeedDataFactory(NetworkInteractor interactor) {
    networkInteractor = interactor;
  }

  @Override public DataSource<Integer, PokemonPreview> create() {
    positionalPokemonDataSource = new PositionalPokemonDataSource(networkInteractor);
    return positionalPokemonDataSource;
  }

  public PositionalPokemonDataSource getPositionalPokemonDataSource() {
    return positionalPokemonDataSource;
  }
}