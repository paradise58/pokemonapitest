package com.paradise.pokemonapitest.pagination;

import androidx.annotation.NonNull;
import androidx.paging.PositionalDataSource;
import com.paradise.pokemonapitest.data.pokemonPreview.PokemonPreview;
import com.paradise.pokemonapitest.domain.NetworkInteractor;
import io.reactivex.disposables.CompositeDisposable;
import java.util.ArrayList;

public class PositionalPokemonDataSource extends PositionalDataSource<PokemonPreview> {
  private NetworkInteractor networkInteractor;
  private final CompositeDisposable compositeDisposable = new CompositeDisposable();
  private int count;

  PositionalPokemonDataSource(NetworkInteractor networkInteractor) {

    this.networkInteractor = networkInteractor;
  }

  @Override public void loadInitial(@NonNull LoadInitialParams params,
      @NonNull LoadInitialCallback<PokemonPreview> callback) {
    compositeDisposable.add(
        networkInteractor.getSortedPokemonPreviewList(0, params.requestedLoadSize).subscribe(s -> {
          count = params.requestedLoadSize;
          callback.onResult(s, 0);
        }, e -> {
          e.printStackTrace();
          callback.onResult(new ArrayList<>(), 0);
        }));
  }

  @Override public void loadRange(@NonNull LoadRangeParams params,
      @NonNull LoadRangeCallback<PokemonPreview> callback) {
    compositeDisposable.add(
        networkInteractor.getSortedPokemonPreviewList(count, params.loadSize).subscribe(s -> {
          count += params.loadSize;
          callback.onResult(s);
        }));
  }

  public void clear() {
    if (!compositeDisposable.isDisposed()) compositeDisposable.clear();
  }
}
