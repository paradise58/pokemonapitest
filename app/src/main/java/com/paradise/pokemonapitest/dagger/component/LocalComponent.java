package com.paradise.pokemonapitest.dagger.component;

import com.paradise.pokemonapitest.dagger.PreviewScope;
import com.paradise.pokemonapitest.dagger.modules.InteractorModule;
import com.paradise.pokemonapitest.dagger.modules.LocalRepositoryModule;
import com.paradise.pokemonapitest.presentation.view.PokemonDetailFragment;
import com.paradise.pokemonapitest.presentation.view.PreviewPokemonFragment;
import dagger.Subcomponent;

@PreviewScope @Subcomponent(modules = {
    LocalRepositoryModule.class, InteractorModule.class
}) public interface LocalComponent {
  void inject(PreviewPokemonFragment previewPokemonFragment);

  void inject(PokemonDetailFragment pokemonDetailFragment);
}
