package com.paradise.pokemonapitest.dagger.modules;

import androidx.lifecycle.ViewModel;
import com.paradise.pokemonapitest.dagger.ViewModelKey;
import com.paradise.pokemonapitest.presentation.viewmodel.DetailFragmentViewModel;
import com.paradise.pokemonapitest.presentation.viewmodel.PreviewFragmentViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module public interface ViewModelModule {

  @Binds @IntoMap @ViewModelKey(PreviewFragmentViewModel.class) ViewModel networkFragmentViewModel(
      PreviewFragmentViewModel viewModel);

  @Binds @IntoMap @ViewModelKey(DetailFragmentViewModel.class) ViewModel detailFragmentViewModel(
      DetailFragmentViewModel viewModel);
}
