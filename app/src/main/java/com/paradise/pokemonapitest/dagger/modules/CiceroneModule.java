package com.paradise.pokemonapitest.dagger.modules;

import com.paradise.pokemonapitest.common.NavigatorHelper;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

@Module public class CiceroneModule {
  private Cicerone<Router> cicerone;

  public CiceroneModule() {
    cicerone = Cicerone.create();
  }

  @Provides @Singleton Router provideRouter() {
    return cicerone.getRouter();
  }

  @Provides @Singleton NavigatorHolder provideNavigationHolder() {
    return cicerone.getNavigatorHolder();
  }

  @Provides @Singleton NavigatorHelper provideNavHelper() {
    return new NavigatorHelper(provideRouter());
  }
}
