package com.paradise.pokemonapitest.dagger.component;

import com.paradise.pokemonapitest.dagger.modules.CiceroneModule;
import com.paradise.pokemonapitest.dagger.modules.ViewModelModule;
import com.paradise.pokemonapitest.presentation.view.MainActivity;
import dagger.Component;
import javax.inject.Singleton;

@Singleton @Component(modules = {
    CiceroneModule.class, ViewModelModule.class
}) public interface AppComponent {
  LocalComponent localComponent();

  NetworkComponent networkComponent();

  void inject(MainActivity mainActivity);
}
