package com.paradise.pokemonapitest.dagger.modules;

import com.paradise.pokemonapitest.common.App;
import com.paradise.pokemonapitest.dagger.PreviewScope;
import com.paradise.pokemonapitest.repositories.NetworkRepo;
import com.paradise.pokemonapitest.repositories.PokemonRepository;
import com.paradise.pokemonapitest.repositories.Retrofit.RetrofitAdapter;
import dagger.Module;
import dagger.Provides;

@Module public class NetworkRepositoryModule {

  @Provides @PreviewScope PokemonRepository provideNetRepo(RetrofitAdapter adapter) {
    return new NetworkRepo(adapter);
  }

  @Provides @PreviewScope RetrofitAdapter provideRetroAdapter() {
    return App.instance.getRetrofitAdapter();
  }
}
