package com.paradise.pokemonapitest.dagger.modules;

import com.paradise.pokemonapitest.dagger.PreviewScope;
import com.paradise.pokemonapitest.domain.NetworkInteractor;
import com.paradise.pokemonapitest.repositories.LocalRepo;
import com.paradise.pokemonapitest.repositories.LocalRepoMethods;
import com.paradise.pokemonapitest.repositories.PokemonRepository;
import dagger.Module;
import dagger.Provides;

@Module public class InteractorModule {

  @Provides @PreviewScope NetworkInteractor provideNetworkInteractor(
      PokemonRepository pokemonRepository, LocalRepoMethods localRepoMethods) {
    return new NetworkInteractor(pokemonRepository, localRepoMethods);
  }

  @Provides @PreviewScope LocalRepoMethods provideLocalSave() {
    return new LocalRepo();
  }
}
