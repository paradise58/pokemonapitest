package com.paradise.pokemonapitest.dagger.modules;

import com.paradise.pokemonapitest.dagger.PreviewScope;
import com.paradise.pokemonapitest.repositories.LocalRepo;
import com.paradise.pokemonapitest.repositories.PokemonRepository;
import dagger.Module;
import dagger.Provides;

@Module public class LocalRepositoryModule {

  @Provides @PreviewScope PokemonRepository provideLocalRepo() {
    return new LocalRepo();
  }
}
